import os
import subprocess
import time
import json
import requests

testUser = "USER_TEST"
ipet_urls = "https://ipet-api-gateway.herokuapp.com/iot-integration/links"

def sendCreatedUrl():
    r = requests.get("http://127.0.0.1:4040/api/tunnels")

    tunnels = r.json()
    publicUrl = tunnels['tunnels'][0]['public_url']
    
    res = requests.post(ipet_urls + '/enable', {
        "userId": testUser,
        "url": publicUrl
    })
    print(res)

    open("publicUrl.txt","w+").write(publicUrl)

def healthCheck():
    publicUrl = open("publicUrl.txt", "r").readline()

    res = requests.get(publicUrl)

    if(res.status_code == 200):
        return True
    else:
        return False

def sendStopUrl():
    publicUrl = open("publicUrl.txt", "r").readline()
    
    res = requests.post(ipet_urls + '/disable', {
        "userId": testUser,
        "url": publicUrl
    })
    print(res)

def onFinish():
    sendStopUrl()
    os.system("taskkill /f /im ngrok.exe")

if __name__ == "__main__":
    
    # enteredIp = input("Enter ip:port of ipwebcam:\n\n")
    enteredIp = open("ip_cam_config.txt", "r").readline()
    ngrokCmd = 'cmd /k "ngrok http -region=sa {0}"'.format(enteredIp)
    proc = subprocess.Popen(ngrokCmd,  shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    sends = False
    while not sends:
        try:
            sendCreatedUrl()
            sends = True
        except Exception:
            print("")
            time.sleep(1)

    try:
        print("Tunnel started!")
        while proc.poll() is None:
            health = healthCheck()
            if(not health):
                break

            time.sleep(10)

        proc.terminate()
        onFinish()

    except KeyboardInterrupt:
        proc.terminate()
        onFinish()
        raise